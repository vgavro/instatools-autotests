import pytest

import selenium


@pytest.fixture
def webdriver():
    return selenium.webdriver.Firefox()


def test_landing(webdriver, variables):
    webdriver.get(variables['landing_url'])
    assert 'INSTA.tools' in webdriver.title
